package com.kike.challenge.controllers;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kike.challenge.models.Usuario;
import com.kike.challenge.repositories.UsuarioRepository;

@CrossOrigin
@RestController
public class UsuarioRestController {
	private final UsuarioRepository usuarioRepository;
	
	@Autowired
	UsuarioRestController(UsuarioRepository usuarioRepository){
		this.usuarioRepository = usuarioRepository;
	}
	
	@GetMapping(value = "/usuario")
	List<Usuario> listaDeTodosLosUsuario(){
		List<Usuario> peliculasEncontradas = usuarioRepository.findAll();
		return peliculasEncontradas;
	}
	
	@PostMapping(value = "/usuario")
	ResponseEntity<Object> crearUsuario(@RequestBody Usuario body){
		Usuario usuarioACrear = Usuario.crearUsuario(body.getNombre(), body.getDireccion());
		Usuario usuarioCreado = usuarioRepository.save(usuarioACrear);
		return new ResponseEntity<Object>(usuarioCreado, HttpStatus.OK);
	}
	
	@GetMapping("/usuario/{id}")
	Optional<Usuario> obtenerUsario(@PathVariable Long id) {
		return this.usuarioRepository.findById(id);
	}
	
	@PutMapping("/usuario/{id}")
	ResponseEntity<Object> actualizarUsuario(@PathVariable Long id, @RequestBody Usuario usuario) {
		Optional<Usuario> opcionalUsuario = this.usuarioRepository.findById(id);
		if(opcionalUsuario.isPresent()) {
			Usuario usuarioEncontrado = opcionalUsuario.get();
			usuarioEncontrado.setNombre(usuario.getNombre());
			usuarioEncontrado.setDireccion(usuario.getDireccion());
			usuarioEncontrado.setPeliculasRentadas(usuario.getPeliculasRentadas());
			Usuario usuarioActualizado = this.usuarioRepository.save(usuarioEncontrado);
			return new ResponseEntity<Object>(usuarioActualizado, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("No se encontró la pelicula" , HttpStatus.NOT_FOUND);
		}
	}
	
	@DeleteMapping(value = "/usuario/{id}")
	ResponseEntity<Object> eliminarUsuario(@PathVariable Long id) {
		Optional<Usuario> usuarioBuscado = this.usuarioRepository.findById(id);
		if(usuarioBuscado.isPresent()){
			Usuario usuarioEncontrado = usuarioBuscado.get();
			this.usuarioRepository.delete(usuarioEncontrado);
			return new ResponseEntity<Object>(usuarioEncontrado, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("No se encontró la pelicula" , HttpStatus.NOT_FOUND);
		}
	}
	
}
