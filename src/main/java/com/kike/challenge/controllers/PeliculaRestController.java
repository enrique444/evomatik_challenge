package com.kike.challenge.controllers;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.kike.challenge.models.Pelicula;
import com.kike.challenge.models.Usuario;
import com.kike.challenge.repositories.PeliculaRepository;
import com.kike.challenge.repositories.UsuarioRepository;

@CrossOrigin
@RestController
public class PeliculaRestController {
	private final PeliculaRepository peliculaRepository;
	private final UsuarioRepository usuarioRepository;
	
	@Autowired
	PeliculaRestController(PeliculaRepository peliculaRepository, UsuarioRepository usuarioRepository){
		this.peliculaRepository = peliculaRepository;
		this.usuarioRepository = usuarioRepository;
	}
	
	@GetMapping(value = "/pelicula")
	List<Pelicula> listaDeTodasLasPeliculas(){
		List<Pelicula> peliculasEncontradas = peliculaRepository.findAll();
		return peliculasEncontradas;
	}

	@PostMapping(value = "/pelicula")
	ResponseEntity<Object> crearPelicula(@RequestBody Pelicula body){
		Pelicula peliculaACrear = Pelicula.crearPelicula(
				body.getNombre(), body.getDescripcion());
		Pelicula pelicula = peliculaRepository.save(peliculaACrear);
		return new ResponseEntity<Object>(pelicula, HttpStatus.OK);
	}
	
	@GetMapping("/pelicula/{id}")
	Optional<Pelicula> obtenerPelicula(@PathVariable Long id) {
		return this.peliculaRepository.findById(id);
	}
	
	@DeleteMapping(value = "/pelicula/{id}")
	ResponseEntity<Object> eliminarPelicula(@PathVariable Long id) {
		Optional<Pelicula> peliculaBuscada = this.peliculaRepository.findById(id);
		if(peliculaBuscada.isPresent()){
			Pelicula peliculaEncontrada = peliculaBuscada.get();
			this.peliculaRepository.delete(peliculaEncontrada);
			return new ResponseEntity<Object>(peliculaEncontrada, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("No se encontró la pelicula" , HttpStatus.NOT_FOUND);
		}
	}
	
	@PutMapping("/pelicula/{id}")
	ResponseEntity<Object> actualizarPelicula(@PathVariable Long id, @RequestBody Pelicula pelicula) {
		Optional<Pelicula> opcionalPelicula = this.peliculaRepository.findById(id);
		if(opcionalPelicula.isPresent()) {
			Pelicula peliculaEncontrada = opcionalPelicula.get();
			peliculaEncontrada.setArrendatario(pelicula.getArrendatario());
			peliculaEncontrada.setDescripcion(pelicula.getDescripcion());
			peliculaEncontrada.setNombre(pelicula.getNombre());
			Pelicula p = this.peliculaRepository.save(peliculaEncontrada);
			return new ResponseEntity<Object>(p, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("No se encontró la pelicula" , HttpStatus.NOT_FOUND);
		}
	}
	
	@GetMapping("/pelicula/rentadas")
	Collection<Pelicula> obtenerPeliculasRentadas() {
		return this.peliculaRepository.findByArrendatarioIsNotNull();
	}
	
	@GetMapping("/pelicula/disponibles")
	Collection<Pelicula> obtenerPeliculasNoRentadas() {
		return this.peliculaRepository.findByArrendatarioIsNull();
	}
	
	@PostMapping(value = "/pelicula/rentar")
	ResponseEntity<Object> rentarPelicula(@RequestBody Pelicula body){
		Optional<Pelicula> opcionalPelicula = this.peliculaRepository.findById(body.getId());
		if(opcionalPelicula.isPresent()) {
			Pelicula peliculaEncontrada = opcionalPelicula.get();
			Usuario arrendatario = body.getArrendatario();
			peliculaEncontrada.setArrendatario(arrendatario);
			Pelicula peliculaRentada = this.peliculaRepository.save(peliculaEncontrada);
			arrendatario = peliculaRentada.getArrendatario();
			arrendatario.incrementarRentasTotales();
			this.usuarioRepository.save(arrendatario);
			return new ResponseEntity<Object>(peliculaRentada, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("No se encontró la pelicula" , HttpStatus.NOT_FOUND);
		}
	}
	
	@PostMapping(value = "/pelicula/devolver")
	ResponseEntity<Object> devolerPelicula(@RequestBody Pelicula body){
		Optional<Pelicula> opcionalPelicula = this.peliculaRepository.findById(body.getId());
		if(opcionalPelicula.isPresent()) {
			Pelicula peliculaEncontrada = opcionalPelicula.get();
			peliculaEncontrada.setArrendatario(null);
			Pelicula peliculaDevuelta = this.peliculaRepository.save(peliculaEncontrada);
			return new ResponseEntity<Object>(peliculaDevuelta, HttpStatus.OK);
		}else {
			return new ResponseEntity<Object>("No se encontró la pelicula" , HttpStatus.NOT_FOUND);
		}
	}
}
