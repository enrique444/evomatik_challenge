package com.kike.challenge.repositories;

import java.util.Collection;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kike.challenge.models.Pelicula;
import com.kike.challenge.models.Usuario;

public interface PeliculaRepository extends JpaRepository<Pelicula, Long> {
	Collection<Pelicula> findByArrendatario(Usuario arrendatario);
	Collection<Pelicula> findByArrendatarioIsNull();
	Collection<Pelicula> findByArrendatarioIsNotNull();
}
