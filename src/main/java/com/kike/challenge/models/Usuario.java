package com.kike.challenge.models;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Usuario {
	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	private String direccion;
	private int rentasTotales;
	@OneToMany(mappedBy = "arrendatario")
	@JsonIgnore
	private Set<Pelicula> peliculasRentadas = new HashSet<>();
	
	private Usuario() {}
	
	private Usuario(String nombre, String direccion) {
		this.nombre = nombre;
		this.direccion = direccion;
	}
	
	public static Usuario crearUsuario(String nombre, String password) {
		return new Usuario(nombre, password);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String password) {
		this.direccion = password;
	}

	public Set<Pelicula> getPeliculasRentadas() {
		return peliculasRentadas;
	}

	public void setPeliculasRentadas(Set<Pelicula> peliculasRentadas) {
		this.peliculasRentadas = peliculasRentadas;
	}
	
	public int getRentasTotales() {
		return this.rentasTotales;
	}
	
	public void setRentasTotales(int rentasTotales) {
		this.rentasTotales = rentasTotales;
	}
	
	public void incrementarRentasTotales() {
		this.rentasTotales = this.rentasTotales + 1;
	}
	
}
