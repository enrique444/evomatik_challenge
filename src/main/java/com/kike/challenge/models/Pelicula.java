package com.kike.challenge.models;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Pelicula {
	@Id
	@GeneratedValue
	private Long id;
	private String nombre;
	private String descripcion;
	@ManyToOne
	private Usuario arrendatario;
	
	private Pelicula() {}
	
	private Pelicula(String nombre, String descripcion) {
		this.nombre = nombre;
		this.descripcion = descripcion;
	}
	
	public static Pelicula crearPelicula(String nombre, String descripcion) {
		return new Pelicula(nombre,  descripcion);
	}
	
	public Usuario getArrendatario() {
		return arrendatario;
	}
	
	public String getNombre() {
		return this.nombre;
	}
	
	public String getDescripcion() {
		return this.descripcion;
	}
	
	public Long getId(){
		return this.id;
	}
	
	public void setNombre(String nombre) {
		this.nombre = nombre;
	}
	
	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
	
	public void setArrendatario(Usuario arrendatario) {
		this.arrendatario = arrendatario;
	}
	
	public double setId(){
		return this.id;
	}
}
